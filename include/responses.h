#ifndef KEYBOARD_SUGGEST_MESSAGES_H_
#define KEYBOARD_SUGGEST_MESSAGES_H_

#include <string>
#include <vector>

#include "state.h"

struct Config;

struct Constraints {
  unsigned passwords_amount = 1;
  unsigned password_length = 8;
  bool include_symbols = true;
  bool include_digits = true;
};

struct PasswordSuggestions {
  std::vector<std::string> passwords;
};

PasswordSuggestions MakePasswordSuggestions(const Constraints &constraints, const Storage &storage);

struct WordSuggestions {
  std::vector<std::string> suggestions;
};

struct Input {
  bool completed = false;
  std::string input;
  std::string last_word;
};

WordSuggestions MakeWordSuggestions(const Input &input, const Storage &storage);

void InitState(const Config &config);

void InitStorage(const Config &config, Storage &storage);

#endif  // KEYBOARD_SUGGEST_MESSAGES_H_
