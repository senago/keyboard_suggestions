#ifndef SUGGESTIONS_H_
#define SUGGESTIONS_H_

#include <queue>
#include <string>

class Suggestions {
 public:
  using WordPair = std::pair<std::string, unsigned>;
  struct CompareSuggestions {
    bool operator()(const WordPair& a, const WordPair& b) { return a.second < b.second; }
  };
  using PQ = std::priority_queue<WordPair, std::vector<WordPair>, CompareSuggestions>;

  Suggestions() { suggestions_ = new PQ; }
  ~Suggestions() { delete suggestions_; }

  WordPair make(std::string word, unsigned frequency) const { return WordPair(std::move(word), frequency); }
  bool empty() const { return suggestions_->empty(); }
  WordPair top() const { return suggestions_->top(); }
  void push(WordPair a) { suggestions_->push(std::move(a)); }
  void pop() { suggestions_->pop(); }

 private:
  PQ* suggestions_ = nullptr;
};

#endif  // SUGGESTIONS_H_
