#ifndef APP_HTTP_SERVICE_H_
#define APP_HTTP_SERVICE_H_

#include <fstream>
#include <functional>
#include <iostream>
#include <memory>
#include <mutex>
#include <restbed>
#include <string>

#include "responses.h"
#include "nlohmann/json.hpp"

class SceneHolder {
 public:
  void Change(WordSuggestions answer) {
    std::lock_guard<std::mutex> lk(mutex_);
    answer_ = std::move(answer);
  }

  void lock() { mutex_.lock(); }

  void unlock() { mutex_.unlock(); }

  /// Thread safe method.
  Input input() const {
    Input input;
    {
      std::lock_guard<std::mutex> lk(mutex_);
      input = input_;
    }
    return input;
  }

  /// Thread safe method.
  WordSuggestions answer() const {
    WordSuggestions answer;
    {
      std::lock_guard<std::mutex> lk(mutex_);
      answer = answer_;
    }
    return answer;
  }

  std::string data() const {
    std::vector<std::string> top_suggestions;
    for (size_t i = 0; i < std::min<size_t>(answer_.suggestions.size(), 10u); ++i) {
      top_suggestions.push_back(answer_.suggestions[i]);
    }
    nlohmann::json js(top_suggestions);
    return js.dump();
  }

 private:
  Input input_;
  WordSuggestions answer_;
  mutable std::mutex mutex_;
};

class HttpService {
 public:
  HttpService(Storage storage, const std::string& viewer_path)
      : storage_(std::move(storage)), viewer_path_(viewer_path) {}

  void Run(uint16_t port);

  void handlePostKeyboard(const std::shared_ptr<restbed::Session> session);
  void handleGetKeyboard(const std::shared_ptr<restbed::Session> session);

  void handlePostPgen(const std::shared_ptr<restbed::Session> session);
  void handleGetPgen(const std::shared_ptr<restbed::Session> session);

 private:
  template <typename handler>
  std::function<void(const std::shared_ptr<restbed::Session>)> getHandlerWrapper(handler h) {
    return std::bind(h, this, std::placeholders::_1);
  }

  std::string GetBody(const restbed::Bytes& bytes) {
    std::string body;
    for (auto b : bytes) {
      body += static_cast<char>(b);
    }
    return body;
  }

  Storage storage_;
  std::string viewer_path_;
  SceneHolder holder_;
};

#endif  // APP_HTTP_SERVICE_H_
