#ifndef APP_APP_H_
#define APP_APP_H_

#include "responses.h"

struct Config {
  size_t port = 0;
  long long timeout = 0;
  std::string data_path;
  std::string resources_path;
};

void Run(const Config&);

#endif  // APP_APP_H_
