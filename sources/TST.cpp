#include "TST.h"

#include <exception>

#include "extra.h"

void TST::LoadFromFile(const std::string& path) {
  std::ifstream file(path);
  try {
    if (!file.is_open()) {
      throw std::runtime_error("couldn't open file: " + path);
    }
  } catch (std::exception& ex) {
    std::cout << "Error's occured: " << ex.what() << std::endl;
    return;
  }

  std::string s;
  while (std::getline(file, s)) {
    size_t pos = s.find(',');
    std::string word = s.substr(0, pos);
    std::string frequency = s.substr(pos + 1);
    if (IsNumber(frequency)) {
      Insert(word, static_cast<unsigned>(std::stoul(frequency)));
    }
  }
}

void TST::Trickle(TST::Node* node, const std::string& key, size_t pos, unsigned frequency) {
  for (size_t i = pos; i < key.size(); ++i) {
    node->mid = new Node(key[i]);
    node = node->mid;
  }

  node->is_word = true;
  node->frequency = frequency;
}

std::pair<TST::Node*, size_t> TST::Traverse(const std::string& key) const {
  TST::Node* node = root_;
  if (node == nullptr) {
    return std::make_pair(node, 0);
  }

  size_t pos = 0;
  while (pos < key.size()) {
    if (key[pos] < node->value) {
      if (node->left == nullptr) {
        return std::make_pair(node, pos);
      } else {
        node = node->left;
      }
    } else if (key[pos] > node->value) {
      if (node->right == nullptr) {
        return std::make_pair(node, pos);
      } else {
        node = node->right;
      }
    } else {
      if (node->mid == nullptr || pos == key.size() - 1) {
        return std::make_pair(node, pos);
      } else {
        node = node->mid;
        ++pos;
      }
    }
  }

  return std::make_pair(node, pos);
}

bool TST::Find(const std::string& key) const {
  if (root_ == nullptr || key.empty()) {
    return false;
  }

  std::pair<TST::Node*, size_t> tr = Traverse(key);
  return tr.first->value == key[key.size() - 1] && tr.second == key.size() - 1 && tr.first->is_word;
}

void TST::Insert(const std::string& key, unsigned frequency) {
  if (root_ == nullptr) {
    root_ = new Node(key[0]);
  }

  TST::Node* node = root_;
  for (size_t i = 0; i < key.size();) {
    if (key[i] < node->value) {
      if (node->left != nullptr) {
        node = node->left;
      } else {
        node->left = new Node(key[i]);
        Trickle(node->left, key, i + 1, frequency);
        return;
      }
    } else if (key[i] > node->value) {
      if (node->right != nullptr) {
        node = node->right;
      } else {
        node->right = new Node(key[i]);
        Trickle(node->right, key, i + 1, frequency);
        return;
      }
    } else {
      if (i == key.size() - 1) {
        node->is_word = true;
        node->frequency = frequency;
        return;
      } else {
        if (node->mid != nullptr) {
          node = node->mid;
          ++i;
        } else {
          Trickle(node, key, i + 1, frequency);
          return;
        }
      }
    }
  }
}

std::vector<std::string> TST::Autocomplete(const std::string& prefix, unsigned amount) {
  if (amount == 0) {
    return {};
  }

  Suggestions suggestions;
  ObtainSuggestions(prefix, suggestions);

  std::vector<std::string> autocomplete_suggestions;
  autocomplete_suggestions.reserve(amount);

  unsigned count = 0;
  while (!suggestions.empty() && count < amount) {
    autocomplete_suggestions.push_back(suggestions.top().first);
    suggestions.pop();
    ++count;
  }

  return autocomplete_suggestions;
}

void TST::ObtainSuggestions(const std::string& prefix, Suggestions& suggestions) {
  std::pair<TST::Node*, size_t> tr = Traverse(prefix);
  TST::Node* node = tr.first;
  if (node == nullptr) return;
  size_t pos = tr.second;

  bool prefix_contains_word = pos == prefix.size() - 1 && node->value == prefix[prefix.size() - 1];
  if (node->is_word && prefix_contains_word) {
    suggestions.push(suggestions.make(prefix.substr(0, pos + 1), node->frequency));
  }

  if (pos == 0) {
    SeekSuggestions(node->mid, std::string(1, node->value), suggestions);
  } else if (prefix_contains_word) {
    SeekSuggestions(node->mid, prefix, suggestions);
  } else {
    SeekSuggestions(node, prefix.substr(0, pos), suggestions);
  }
}

void TST::SeekSuggestions(TST::Node* node, const std::string& prefix, Suggestions& suggestions) {
  if (node == nullptr) return;

  if (node->is_word) {
    suggestions.push(suggestions.make(prefix + node->value, node->frequency));
  }

  SeekSuggestions(node->left, prefix, suggestions);
  SeekSuggestions(node->right, prefix, suggestions);
  SeekSuggestions(node->mid, prefix + node->value, suggestions);
}

void TST::DeleteNodes(TST::Node* node) {
  if (node == nullptr) return;

  DeleteNodes(node->left);
  DeleteNodes(node->mid);
  DeleteNodes(node->right);

  delete node;
}
