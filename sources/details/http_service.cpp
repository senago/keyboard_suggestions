#include "details/http_service.h"

void HttpService::Run(uint16_t port) {
  // ------------------------- Keyboard ------------------------- //
  auto keyboard_viewer_resource = std::make_shared<restbed::Resource>();
  keyboard_viewer_resource->set_path("/viewer/{filename: (keyboard.html|keyboard.js|style.css)}");
  keyboard_viewer_resource->set_method_handler("GET", getHandlerWrapper(&HttpService::handleGetKeyboard));

  auto keyboard_input_resource = std::make_shared<restbed::Resource>();
  keyboard_input_resource->set_path("/keyboard");
  keyboard_input_resource->set_method_handler("POST", getHandlerWrapper(&HttpService::handlePostKeyboard));
  // -------------------------------------------------- //

  // ------------------------- Pgen ------------------------- //
  auto pgen_input_resource = std::make_shared<restbed::Resource>();
  pgen_input_resource->set_path("/pgen");
  pgen_input_resource->set_method_handler("POST", getHandlerWrapper(&HttpService::handlePostPgen));

  auto pgen_viewer_resource = std::make_shared<restbed::Resource>();
  pgen_viewer_resource->set_path("/viewer/{filename: (pgen.html|pgen.js|style.css)}");
  pgen_viewer_resource->set_method_handler("GET", getHandlerWrapper(&HttpService::handleGetPgen));
  // -------------------------------------------------- //

  auto settings = std::make_shared<restbed::Settings>();
  settings->set_port(port);
  settings->set_default_header("Connection", "close");

  std::cout << "Press Ctrl+C to stop the service." << std::endl;
  const std::string full_url = "http://localhost:" + std::to_string(port) + "/viewer/keyboard.html";
  std::cout << std::endl << "Staring service: " << full_url << std::endl;

  restbed::Service service;
  service.publish(keyboard_viewer_resource);
  service.publish(keyboard_input_resource);
  service.publish(pgen_viewer_resource);
  service.publish(pgen_input_resource);
  service.start(settings);
}
