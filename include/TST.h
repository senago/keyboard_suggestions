#ifndef TST_H_
#define TST_H_

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "suggestions.h"

class TST {
 public:
  TST() = default;
  void LoadFromFile(const std::string& path);
  std::vector<std::string> Autocomplete(const std::string& prefix, unsigned amount);
  ~TST() { DeleteNodes(root_); }

 private:
  void Insert(const std::string& key, unsigned frequency);

  struct Node {
    char value = ' ';
    bool is_word = false;
    Node* left = nullptr;
    Node* mid = nullptr;
    Node* right = nullptr;
    unsigned frequency = 0;

    Node(char c, bool is_word = false, Node* left = nullptr, Node* mid = nullptr, Node* right = nullptr,
         unsigned frequency = 0)
        : value(c), is_word(is_word), left(left), mid(mid), right(right), frequency(frequency){};
  };

  bool Find(const std::string& key) const;
  void SeekSuggestions(Node* node, const std::string& prefix, Suggestions& suggestions);
  void ObtainSuggestions(const std::string& prefix, Suggestions& suggestions);
  void Trickle(Node* node, const std::string& key, size_t pos, unsigned frequency);
  std::pair<Node*, size_t> Traverse(const std::string& key) const;
  void DeleteNodes(Node* node);

  Node* root_ = nullptr;
};

#endif  // TST_H_
