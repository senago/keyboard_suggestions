#include "NGrams.h"

#include <exception>

#include "extra.h"

void NGrams::LoadFromFiles(const std::string& two_words_path, const std::string& three_words_path,
                           const std::string& four_words_path, const std::string& five_words_path) {
  LoadFromFile(two_words_hash_table_, two_words_path);
  LoadFromFile(three_words_hash_table_, three_words_path);
  LoadFromFile(four_words_hash_table_, four_words_path);
  LoadFromFile(five_words_hash_table_, five_words_path);
}

void NGrams::LoadFromFile(NGramsHashTable& hash_table, const std::string& path) {
  std::ifstream file(path);
  try {
    if (!file.is_open()) {
      throw std::runtime_error("couldn't open file: " + path);
    }
  } catch (std::exception& ex) {
    std::cout << "Error's occured: " << ex.what() << std::endl;
    return;
  }

  std::string size;
  std::getline(file, size);
  hash_table.reserve(static_cast<size_t>(std::stoul(size)));

  NGramPairsVector word_pairs;
  word_pairs.reserve(3);
  std::vector<std::string> frequences;
  frequences.reserve(3);

  CSVReader csv_reader;
  while (csv_reader.Parse(file)) {
    frequences = Split(csv_reader.at(4), " ");
    for (size_t i = 0; i < frequences.size(); ++i) {
      if (csv_reader.at(i + 1).empty()) continue;
      unsigned frequency = static_cast<unsigned>(std::stoul(frequences[i]));
      word_pairs.push_back({csv_reader.at(i + 1), frequency});
    }
    hash_table.insert({csv_reader.at(0), std::make_shared<NGramPairsVector>(word_pairs)});
    word_pairs.clear();
  }
}

std::vector<std::string> NGrams::GetNextWordPredictions(const std::string& input, unsigned amount) const {
  Suggestions suggestions;
  std::vector<std::string> last_words = GetLastWords(input, kMaxLeadingPhraseLength);
  Predict(last_words, two_words_hash_table_, suggestions, 1);
  Predict(last_words, three_words_hash_table_, suggestions, 2);
  Predict(last_words, four_words_hash_table_, suggestions, 3);
  Predict(last_words, five_words_hash_table_, suggestions, 4);

  std::vector<std::string> next_word_predictions;
  next_word_predictions.reserve(amount);

  unsigned count = 0;
  while (!suggestions.empty() && count < amount) {
    next_word_predictions.push_back(suggestions.top().first);
    suggestions.pop();
    ++count;
  }

  return next_word_predictions;
}

void NGrams::Predict(const std::vector<std::string>& last_words, const NGramsHashTable& hash_table,
                     Suggestions& suggestions, size_t leading_phrase_length) const {
  size_t words_amount = last_words.size();
  if (leading_phrase_length > words_amount) return;

  std::string leading_phrase;
  size_t i = words_amount - leading_phrase_length;
  while (i < words_amount) {
    leading_phrase += last_words[i];
    ++i;
    if (i != words_amount) {
      leading_phrase += ' ';
    }
  }

  auto it = hash_table.find(leading_phrase);
  if (it == hash_table.end()) return;
  for (const auto& ngram_pair : *(*it).second) {
    suggestions.push(suggestions.make(ngram_pair.first, ngram_pair.second));
  }
}
