#ifndef NGRAMS_H_
#define NGRAMS_H_

#include <fstream>
#include <iostream>
#include <memory>
#include <unordered_map>

#include "CSVReader.h"
#include "suggestions.h"

class NGrams {
 public:
  using NGramPair = std::pair<std::string, unsigned>;
  using NGramPairsVector = std::vector<NGramPair>;
  using NGramsHashTable = std::unordered_map<std::string, std::shared_ptr<NGramPairsVector>>;

  NGrams() = default;
  void LoadFromFiles(const std::string& two_words_path, const std::string& three_words_path,
                     const std::string& four_words_path, const std::string& five_words_path);
  std::vector<std::string> GetNextWordPredictions(const std::string& input, unsigned amount) const;

  const unsigned kMaxLeadingPhraseLength = 4;

 private:
  void LoadFromFile(NGramsHashTable& hash_table, const std::string& path);
  void Predict(const std::vector<std::string>& last_words, const NGramsHashTable& hash_table, Suggestions& suggestions,
               size_t leading_phrase_length) const;

  NGramsHashTable two_words_hash_table_;
  NGramsHashTable three_words_hash_table_;
  NGramsHashTable four_words_hash_table_;
  NGramsHashTable five_words_hash_table_;
};

#endif  // NGRAMS_H_
