#include "isaac.h"

const static std::string digits = "0123456789";
const static std::string lowercase = "abcdefghijklmnopqrstuvwxyz";
const static std::string uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const static std::string symbols = "!@#$&*|";

bool hasDigit(const std::string& s) {
  for (char c : s) {
    if (c >= '0' && c <= '9') {
      return true;
    }
  }
  return false;
}

bool hasSymbol(const std::string& s) {
  for (char c : s) {
    if (symbols.find(c) != symbols.size()) {
      return true;
    }
  }
  return false;
}

std::vector<std::string> isaac::GetPasswords(const Constraints& constraints) {
  std::string charset = lowercase + uppercase;
  if (constraints.include_digits) {
    charset += digits;
  }
  if (constraints.include_symbols) {
    charset += symbols;
  }

  std::string pw;
  std::size_t index = constraints.password_length;
  std::vector<std::string> passwords;
  for (unsigned i = 0; i < constraints.passwords_amount; ++i) {
    for (unsigned j = 0; j < constraints.password_length; ++j) {
      pw += charset[getRand() % charset.size()];
    }
    if (constraints.include_digits && !hasDigit(pw)) {
      index = getRand() % pw.size();
      pw[index] = digits[getRand() % digits.size()];
    }
    if (constraints.include_symbols && !hasSymbol(pw)) {
      std::size_t shift = getRand() % pw.size();
      if (shift == 0 && index == constraints.password_length) {
        index = (index + 1) % pw.size();
      } else {
        index = (index + shift) % pw.size();
      }
      pw[index] = symbols[getRand() % symbols.size()];
    }
    passwords.push_back(pw);
    pw.clear();
  }

  return passwords;
}
