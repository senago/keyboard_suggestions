// -------------------------------- Constants -------------------------------- //

const input_include_symbols = document.getElementById("include_symbols"),
    input_include_digits = document.getElementById("include_digits"),
    input_passwords_amount = document.getElementById("passwords_amount"),
    input_password_length = document.getElementById("password_length"),
    passwords_container = document.getElementById("passwords-container"),
    download_button = document.getElementById("download_button");

var current_passwords;

// -------------------------------- Common -------------------------------- //

function XHRrequest(method, url, data, f) {
    return new Promise(function(resolve) {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.overrideMimeType("text/html");
        xhr.onreadystatechange = function(e) {
            if (xhr.readyState === 4 && xhr.status == 200) {
                resolve(xhr.response);
                f(xhr.response);
            }
        }
        xhr.send(data);
    })
}

// -------------------------------- Functions -------------------------------- //

function getPasswords() {
    if (!validate(input_passwords_amount.value, input_passwords_amount.min, input_passwords_amount.max)
    ||  !validate(input_password_length.value, input_password_length.min, input_password_length.max)) {
        return;
    }
    XHRrequest("POST", "/pgen", JSON.stringify({
        include_symbols: input_include_symbols.checked,
        include_digits: input_include_digits.checked,
        passwords_amount: input_passwords_amount.valueAsNumber,
        password_length: input_password_length.valueAsNumber,
    }), applyData);
}

function applyData(response) {
    passwords_container.innerHTML = "";
    current_passwords = JSON.parse(response);
    current_passwords.forEach(function(v) {
        password = document.createElement("span");
        password.innerText = v;
        passwords_container.appendChild(password);
    })
    download_button.style.visibility = "visible";
}

function downloadJSON() {
    let filename = "passwords.json";
    let blob = new Blob([JSON.stringify(current_passwords)], {type: "application/json"});
    if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    } else {
        let elem = window.document.createElement("a");
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;
        document.body.appendChild(elem);
        elem.click();        
        document.body.removeChild(elem);
        window.URL.revokeObjectURL(blob);
    }
}

function validate(n, min, max) {
    nn = parseInt(n, 10);
    minn = parseInt(min, 10);
    maxx = parseInt(max, 10);
    return nn >= minn && nn <=  maxx;
}