#ifndef EXTRA_H_
#define EXTRA_H_

#include <iostream>
#include <string>
#include <vector>

inline std::vector<std::string> Split(const std::string& s, const std::string& sep) {
  std::vector<std::string> v;
  size_t previous = 0;
  size_t current = 0;
  while ((current = s.find(sep, current)) != std::string::npos) {
    v.push_back(s.substr(previous, current - previous));
    current += sep.size();
    previous = current;
  }
  v.push_back(s.substr(previous));
  return v;
}

inline std::vector<std::string> GetLastWords(const std::string& s, unsigned amount) {
  std::vector<std::string> words = Split(s, " ");
  if (amount > words.size()) amount = words.size();
  return std::vector<std::string>(words.end() - amount, words.end());
}

inline bool IsNumber(const std::string& s) {
  auto it = s.begin();
  while (it != s.end() && std::isdigit(*it)) ++it;
  return !s.empty() && it == s.end();
}

#endif  // EXTRA_H_
