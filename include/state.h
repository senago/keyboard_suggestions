#ifndef KEYBOARD_SUGGEST_STATE_H_
#define KEYBOARD_SUGGEST_STATE_H_

#include "NGrams.h"
#include "TST.h"
#include "isaac.h"

struct InternalState {};

class isaac;

struct Storage {
  isaac* isaac_;

  TST* TST_;
  NGrams* NGrams_;
  const unsigned kAmountOfWords = 10;
};

#endif  // KEYBOARD_SUGGEST_STATE_H_
