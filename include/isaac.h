//  taken from:
//    https://github.com/sjinks/cpp_isaac64
//  which borrows algorithm from Offical ISAAC site:
//    https://www.burtleburtle.net/bob/rand/isaacafa.html

#ifndef ISAAC_H
#define ISAAC_H

#include <cstdint>
#include <ctime>
#include <limits>
#include <random>
#include <string>

#include "responses.h"

struct Constraints;

class isaac {
 public:
  using ui32 = std::uint32_t;
  using ui8 = std::uint8_t;
  const static unsigned long long randsize = 1ULL << 8;

  isaac() : m_RV_count(0), m_RV(), m_mm(), m_aa(0), m_bb(0), m_cc(0) {
    std::seed_seq seed_seq{std::time(nullptr)};
    seed_seq.generate(m_RV, m_RV + randsize);
    init();
  }

  std::vector<std::string> GetPasswords(const Constraints& constraints);

 private:
  ui32 getRand() {
    if (m_RV_count == 0) {
      isaac32();
      m_RV_count = randsize;
    }
    --m_RV_count;
    return m_RV[m_RV_count];
  }

  void init() {
    std::size_t i;
    ui32 a = 0x9E3779B9;
    ui32 b = 0x9E3779B9;
    ui32 c = 0x9E3779B9;
    ui32 d = 0x9E3779B9;
    ui32 e = 0x9E3779B9;
    ui32 f = 0x9E3779B9;
    ui32 g = 0x9E3779B9;
    ui32 h = 0x9E3779B9;

    m_aa = 0;
    m_bb = 0;
    m_cc = 0;

    for (i = 0; i < 4; ++i) {
      mix(a, b, c, d, e, f, g, h);
    }

    for (i = 0; i < randsize; i += 8) {
      a += m_RV[i + 0];
      b += m_RV[i + 1];
      c += m_RV[i + 2];
      d += m_RV[i + 3];
      e += m_RV[i + 4];
      f += m_RV[i + 5];
      g += m_RV[i + 6];
      h += m_RV[i + 7];

      mix(a, b, c, d, e, f, g, h);

      m_mm[i + 0] = a;
      m_mm[i + 1] = b;
      m_mm[i + 2] = c;
      m_mm[i + 3] = d;
      m_mm[i + 4] = e;
      m_mm[i + 5] = f;
      m_mm[i + 6] = g;
      m_mm[i + 7] = h;
    }

    for (i = 0; i < randsize; i += 8) {
      a += m_mm[i + 0];
      b += m_mm[i + 1];
      c += m_mm[i + 2];
      d += m_mm[i + 3];
      e += m_mm[i + 4];
      f += m_mm[i + 5];
      g += m_mm[i + 6];
      h += m_mm[i + 7];

      mix(a, b, c, d, e, f, g, h);

      m_mm[i + 0] = a;
      m_mm[i + 1] = b;
      m_mm[i + 2] = c;
      m_mm[i + 3] = d;
      m_mm[i + 4] = e;
      m_mm[i + 5] = f;
      m_mm[i + 6] = g;
      m_mm[i + 7] = h;
    }

    isaac32();
    m_RV_count = randsize;
  }

  static void mix(ui32& a, ui32& b, ui32& c, ui32& d, ui32& e, ui32& f, ui32& g, ui32& h) {
    a ^= b << 11;
    d += a;
    b += c;
    b ^= c >> 2;
    e += b;
    c += d;
    c ^= d << 8;
    f += c;
    d += e;
    d ^= e >> 16;
    g += d;
    e += f;
    e ^= f << 10;
    h += e;
    f += g;
    f ^= g >> 4;
    a += f;
    g += h;
    g ^= h << 8;
    b += g;
    h += a;
    h ^= a >> 9;
    c += h;
    a += b;
  }

  void isaac32() {
    ui32 x, y;
    std::size_t i = 0;

    ++m_cc;
    m_bb += m_cc;

    while (i < randsize) {
      x = m_mm[i];
      switch (i) {
        case 0:
          m_aa ^= m_aa << 13;
          break;
        case 1:
          m_aa ^= m_aa >> 6;
          break;
        case 2:
          m_aa ^= m_aa << 2;
          break;
        case 3:
          m_aa ^= m_aa >> 16;
          break;
      }

      m_aa += m_mm[static_cast<ui8>(i + 128) % randsize];
      m_mm[i] = y = m_mm[static_cast<ui8>(x >> 2) % randsize] + m_aa + m_bb;
      m_RV[i] = m_bb = m_mm[static_cast<ui8>(y >> 10)] + x;

      ++i;
    }
  }

  isaac(const isaac&) = delete;
  isaac& operator=(const isaac&) = delete;

  ui32 m_aa = 0;
  ui32 m_bb = 0;
  ui32 m_cc = 0;
  ui32 m_mm[256];
  ui32 m_RV[256];
  std::size_t m_RV_count = 0;
};

#endif  // ISAAC_H
