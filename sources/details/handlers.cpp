#include "details/http_service.h"

#include <sstream>

namespace {
std::string GetLastWord(const std::string &input) {
  std::stringstream s(input);
  std::string last_word;
  while (!s.eof()) {
    std::string t;
    s >> t;
    if (!t.empty()) {
      last_word = std::move(t);
    }
  }
  return last_word;
}
}  // namespace

// ------------------------- Keyboard ------------------------- //
void HttpService::handleGetKeyboard(const std::shared_ptr<restbed::Session> session) {
  const auto request = session->get_request();
  const std::string filename = request->get_path_parameter("filename");

  std::ifstream stream(viewer_path_ + filename, std::ifstream::in);
  if (stream.is_open()) {
    const std::string body = std::string(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());
    session->close(restbed::OK, body);
  } else {
    session->close(restbed::NOT_FOUND);
  }
}

void HttpService::handlePostKeyboard(const std::shared_ptr<restbed::Session> session) {
  const auto request = session->get_request();
  int content_length = request->get_header("Content-Length", 0);
  session->fetch(content_length, [this](const std::shared_ptr<restbed::Session> session, const restbed::Bytes &body) {
    nlohmann::json JSON = nlohmann::json::parse(GetBody(body));
    Input input;
    JSON.at("input").get_to(input.input);
    input.last_word = GetLastWord(input.input);
    JSON.at("completed").get_to(input.completed);

    auto answer = MakeWordSuggestions(input, storage_);
    holder_.Change(std::move(answer));

    std::unique_lock<SceneHolder> lk(holder_);
    session->close(restbed::OK, holder_.data(), {{"Content-Type", "application/json"}});
  });
}
// -------------------------------------------------- //

// ------------------------- Pgen ------------------------- //
void HttpService::handleGetPgen(const std::shared_ptr<restbed::Session> session) {
  const auto request = session->get_request();
  const std::string filename = request->get_path_parameter("filename");

  std::ifstream stream(viewer_path_ + filename, std::ifstream::in);
  if (stream.is_open()) {
    const std::string body = std::string(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());
    session->close(restbed::OK, body);
  } else {
    session->close(restbed::NOT_FOUND);
  }
}

void HttpService::handlePostPgen(const std::shared_ptr<restbed::Session> session) {
  const auto request = session->get_request();
  int content_length = request->get_header("Content-Length", 0);
  session->fetch(content_length, [this](const std::shared_ptr<restbed::Session> session, const restbed::Bytes &body) {
    nlohmann::json JSON = nlohmann::json::parse(GetBody(body));
    Constraints constraints;
    JSON.at("include_symbols").get_to(constraints.include_symbols);
    JSON.at("include_digits").get_to(constraints.include_digits);
    JSON.at("passwords_amount").get_to(constraints.passwords_amount);
    JSON.at("password_length").get_to(constraints.password_length);

    std::vector<std::string> password_suggestions = MakePasswordSuggestions(constraints, storage_).passwords;
    nlohmann::json js(password_suggestions);

    session->close(restbed::OK, js.dump(), {{"Content-Type", "application/json"}});
  });
}
// -------------------------------------------------- //
