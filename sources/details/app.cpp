#include "details/app.h"

#include <iostream>
#include <string>
#include <thread>

#include "details/http_service.h"
#include "responses.h"

void Run(const Config &config) {
  InitState(config);

  Storage storage;
  InitStorage(config, storage);

  HttpService service(std::move(storage), config.resources_path);

  std::thread service_runner(&HttpService::Run, &service, config.port);
  service_runner.join();
}
