// -------------------------------- Constants -------------------------------- //

const suggestionsList = document.getElementById("suggestions"),
    input = document.getElementById("input_text");

// -------------------------------- Common -------------------------------- //

function XHRrequest(method, url, data, f) {
    return new Promise(function(resolve) {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.overrideMimeType("text/html");
        xhr.onreadystatechange = function(e) {
            if (xhr.readyState === 4 && xhr.status == 200) {
                resolve(xhr.response);
                f(xhr.response);
            }
        }
        xhr.send(data);
    })
}

// -------------------------------- Functions -------------------------------- //

function onInputChanged() {
    XHRrequest("POST", "/keyboard", JSON.stringify({
        input: sanitize(input.value),
        completed: input.value.endsWith(" "),
    }), applyData);
}

function sanitize(s) {
    return s.trim().replace(/\s+/g, ' ').trim().toLowerCase();
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function applyData(response) {
    while (suggestionsList.firstChild) {
        suggestionsList.firstChild.remove();
    }

    let data = JSON.parse(response);
    data.filter(onlyUnique).forEach(function(v) {
        let suggestion = document.createElement("input");
        suggestion.value = v;
        suggestion.type = "button";
        suggestion.id = "suggestion";
        suggestion.onclick = onButtonClick;
        suggestionsList.appendChild(suggestion);
    })
}

function onButtonClick(event) {
    let text = document.getElementById("input_text").value;
    let words = text.split(' ');
    words[words.length - 1] = event.target.value;
    document.getElementById("input_text").value = words.join(' ') + ' ';
    onInputChanged(null);
}

onInputChanged(null);