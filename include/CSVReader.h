#ifndef CSVReader_H_
#define CSVReader_H_

#include <fstream>
#include <string>
#include <vector>

class CSVReader {
 public:
  void ReadNextLine(std::istream& stream) {
    std::getline(stream, line_);

    positions_.clear();
    positions_.push_back(0);
    std::string::size_type pos = 0;
    size_t delimeter_length = delimeter_.length();
    while ((pos = line_.find(delimeter_, pos)) != std::string::npos) {
      positions_.push_back(pos + delimeter_length - 1);
      ++pos;
    }

    positions_.push_back(line_.size());
  }

  std::istream& Parse(std::istream& stream) {
    ReadNextLine(stream);
    return stream;
  }

  std::string at(std::size_t index) const {
    return line_.substr(positions_[index] + 1, positions_[index + 1] - (positions_[index] + delimeter_.length()));
  }

  size_t size() const { return positions_.size(); }

 private:
  const std::string delimeter_ = "\",\"";
  std::string line_;
  std::vector<int> positions_;
};

#endif  // CSVReader_H_
