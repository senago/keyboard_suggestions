### How to start

Get all the dependencies
```shell script
git submodule update --init --recursive
```

Build the project
```shell script
cmake -B _builds -DBUILD_SSL=NO
cmake --build _builds --target app
```

Start the app
```shell script
./_builds/app -r ./misc/viewer/
```