#include "responses.h"

#include "details/app.h"

void InitState(const Config &config) {}

void InitStorage(const Config &config, Storage &storage) {
  storage.isaac_ = new isaac();

  storage.TST_ = new TST();
  storage.TST_->LoadFromFile(config.data_path + "data.txt");

  storage.NGrams_ = new NGrams();
  storage.NGrams_->LoadFromFiles(config.data_path + "two_words.csv", config.data_path + "three_words.csv",
                                 config.data_path + "four_words.csv", config.data_path + "five_words.csv");
}

PasswordSuggestions MakePasswordSuggestions(const Constraints &constraints, const Storage &storage) {
  if (constraints.password_length <= 0 || constraints.passwords_amount <= 0) {
    return PasswordSuggestions{};
  }
  return PasswordSuggestions{storage.isaac_->GetPasswords(constraints)};
}

WordSuggestions MakeWordSuggestions(const Input &input, const Storage &storage) {
  if (input.input.size() == 0) {
    return WordSuggestions{};
  }

  std::vector<std::string> suggestions;
  if (input.completed) {
    suggestions = storage.NGrams_->GetNextWordPredictions(input.input, storage.kAmountOfWords);
  } else {
    suggestions = storage.TST_->Autocomplete(input.last_word, storage.kAmountOfWords);
  }

  return WordSuggestions{suggestions};
}
